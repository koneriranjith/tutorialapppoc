var STEPGUID = (function () {
  let data;

  function removeElement(clas) {
    var elem = document.querySelector(clas);
    elem.parentNode.removeChild(elem);
  }

  function addCssRule(selector, rule) {
    if (document.styleSheets) {
      if (!document.styleSheets.length) {
        var head = document.getElementsByTagName('head')[0];
        head.appendChild(bc.createEl('style'));
      }
      var i = document.styleSheets.length - 1;
      var ss = document.styleSheets[i];
      var l = 0;
      if (ss.cssRules) {
        l = ss.cssRules.length;
      } else if (ss.rules) {
        // IE
        l = ss.rules.length;
      }
      if (ss.insertRule) {
        ss.insertRule(selector + ' {' + rule + '}', l);
      } else if (ss.addRule) {
        // IE
        ss.addRule(selector, rule, l);
      }
    }
  };

  function _next(selector, next) {
    removeElement('.myOverlay');
    removeElement('.toolTip');
    document.querySelector(selector).classList.remove("highlight");
    showOverlay(--next);
  }

  function _end(selector) {
    removeElement('.myOverlay');
    removeElement('.toolTip');
    document.querySelector(selector).classList.remove("highlight");
    data = '';
    localStorage.setItem("STEPGUID", true);
  }

  function showOverlay(stepCount) {
    let {
      steps
    } = data;
    let step = steps[stepCount];
    let content = "";
    // overlay div created
    let myOverlay = document.createElement('div');
    myOverlay.setAttribute('class', 'myOverlay');
    document.querySelector('.container').appendChild(myOverlay);
    document.querySelector(step.selector).classList.add("highlight");
    // tool tip element created
    let toolTipEle = document.createElement('div');
    toolTipEle.setAttribute('class', 'toolTip');

    // tool tip arrow position set starts
    switch (step.tipPosition) {
      case 'top':
        addCssRule('.toolTip::before', `left: ${(step.width.replace('px', '')/2)}px; top: -4px; transform: rotate(45deg);`)
        break;
      case 'right':
        addCssRule('.toolTip::before', `left: ${(parseInt(step.width.replace('px', ''))+10)}px; top: ${(step.height.replace('px', '')/2)}px; transform: rotate(45deg);`)
        break;
      case 'bottom':
        addCssRule('.toolTip::before', `left: 50px; top: ${(parseInt(step.height.replace('px', ''))+10)}px; transform: rotate(45deg);`)
        break;
      default:
        break;
    }
    // tool tip arrow position set end

    toolTipEle.style.top = step.top;
    toolTipEle.style.left = step.left;
    toolTipEle.style.width = step.width;
    toolTipEle.style.height = step.height;
    content = step.next ? `${step.content}<br> <a href="javascript:void(0)" id="btnNext">Next</a>` :
      `${step.content}${step.content}<br> <a href="javascript:void(0)" id="btnEnd">End</a>`;
    toolTipEle.innerHTML = content;
    document.querySelector('.container').appendChild(toolTipEle);
    let btnNextEle = document.getElementById("btnNext");
    let btnEndEle = document.getElementById("btnEnd");
    if (btnNextEle) {
      btnNextEle.addEventListener("click", function () {
        _next(step.selector, step.next);
      }, false);
    } else if (btnEndEle) {
      document.getElementById("btnEnd").addEventListener("click", function () {
        _end(step.selector);
      }, false);
    }
  }

  function statusCheck() {
    return localStorage.getItem("STEPGUID");
  }

  function clear() {
    return localStorage.removeItem("STEPGUID");
  }

  function init(inputData) {
    if (!data && inputData && inputData.steps && inputData.steps.length) {
      data = inputData;
      let minData = data.steps.reduce(
        (acc, loc) => {
          return acc.id < loc.id ? acc : loc
        });
      showOverlay(--minData.id);
    } else if (!inputData || !inputData.steps || !inputData.steps.length) {
      console.log("Input data is empty!");
      throw (new Error('Input data is empty!'))
    } else if (data) {
      console.log("STEPGUID initialized!");
    } else {
      console.log("something went wrong!");
      throw (new Error('Something went Wrong'))
    }
    return
  }
  return {
    init: init,
    status: statusCheck,
    clear: clear
  }
})();
module.exports = STEPGUID;
