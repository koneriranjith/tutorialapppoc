var STEPGUID = require('./lib.js');
require("jasmine-local-storage");

describe('Testing all the functions to be called', () => {
  beforeEach(() => {
    var store = {};
    spyOn(STEPGUID, 'init')
    spyOn(STEPGUID, 'clear')
    spyOn(localStorage, 'getItem').and.callFake((key) => {
      return store[key] || null;
    });
    spyOn(localStorage, 'setItem').and.callFake((key, value) => {
      return store[key] = value;
    });
  });
  it('should call Init', () => {
    let data = {
      "steps": [{
        "id": "1",
        "content": "tip on first div",
        "selector": ".myClass1",
        "next": 2,
        "left": "400px",
        "top": "120px",
        "width": "100px",
        "tipPosition": "top",
        "height": "100px"
      }, {
        "id": "2",
        "content": "tip on second div",
        "selector": ".myClass2",
        "next": 3,
        "left": "856px",
        "top": "144px",
        "width": "100px",
        "tipPosition": "right",
        "height": "100px"
      }, {
        "id": "3",
        "content": "tip on third div.",
        "selector": ".myClass3",
        "next": null,
        "left": "100px",
        "top": "431px",
        "width": "100px",
        "tipPosition": "bottom",
        "height": "100px"
      }]
    }
    STEPGUID.init(data)
    expect(STEPGUID.init).toHaveBeenCalled()
  })
  it('should set an Item', () => {
    expect(localStorage.setItem("STEPGUID", true)).toBe(true); // true
    var returnValue = STEPGUID.status();
    expect(returnValue).toBeTruthy();
  });
  it('Should call clear and return null', () => {
    STEPGUID.clear()
    expect(STEPGUID.clear).toHaveBeenCalled();
    var returnValue = STEPGUID.status();
    expect(returnValue).toBe(null);
  })
})

describe('Testing Error Functions', () => {
  it('should throw error in init if data is undefined', () => {
    expect(function () {
      STEPGUID.init(undefined)
    }).toThrowError("Input data is empty!");
  })
});
